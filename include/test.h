#ifndef _TEST_H_
#define _TEST_H_

void test_list();
void testTree();
void testTab();
void testHeapList();
void testHeapTree();
void testHeapDynTab();

#endif // _TEST_H_
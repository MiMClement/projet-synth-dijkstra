#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "tree.h"

TNode * newTNode(void* data) {
	TNode * N = malloc(sizeof(TNode));
	N->data = data;
	N->left = NULL;
	N->parent = NULL;
	N->right = NULL;
	return N;
}

CBTree * newCBTree() {
	CBTree * T = malloc(sizeof(CBTree));
	T->numelm = 0;
	T->last = NULL;
	T->root = NULL;
	return T;
}

static void preorder(TNode *node, void (*ptrF)(const void*)) {
	if (node != NULL) {
		ptrF(node->data);
		printf(" ");
		preorder(node->left, ptrF);
		preorder(node->right, ptrF);
	}
}

static void inorder(TNode *node, void (*ptrF)(const void*)) {
	if (node != NULL) {
		inorder(node->left, ptrF);
		ptrF(node->data);
		printf(" ");
		inorder(node->right, ptrF);
	}
}

static void postorder(TNode *node, void (*ptrF)(const void*)) {
	if (node != NULL) {
		postorder(node->left, ptrF);
		postorder(node->right, ptrF);
		ptrF(node->data);
		printf(" ");
	}
}

// order = 0 (preorder), 1 (postorder), 2 (inorder)
void viewCBTree(const CBTree* tree, void (*ptrF)(const void*), int order) {
	assert(order == 0 || order == 1 || order == 2);
	printf("nb of nodes = %d\n", tree->numelm);
	switch (order) {
		case 0:
			preorder(tree->root, ptrF);
			break;
		case 1:
			postorder(tree->root, ptrF);
			break;
		case 2:
			inorder(tree->root, ptrF);
		break;
	}
	printf("\n");
}

void CBTreeInsert(CBTree* tree, void* data) {
	TNode * E;
	
	if (tree->numelm == 0) {		//Si l'arbre est vide, le nouveau noeud est la racine
		tree->root = newTNode(data);
		tree->last = tree->root;
		tree->numelm = 1;
	} else if (tree->numelm == 1) {	//Si l'arbre contient un seul élément, le nouveau noeud est donc le fils gauche de la racine
		tree->root->left = newTNode(data);
		tree->last = tree->root->left;
		tree->last->parent = tree->root;
		tree->numelm = 2;
	} else if (tree->last == tree->last->parent->left) {	//Si le noeud last est le fils gauche de son parent, alors le nouveau noeud est le fils droit de ce parent
		tree->last->parent->right = newTNode(data);
		tree->last->parent->right->parent = tree->last->parent;
		tree->last = tree->last->parent->right;
		tree->numelm++;
	} else if (tree->last == tree->last->parent->right) {	//Si le noeud last est le fils droit de son parent, il faut remonter les parents tant que le fils est à droite du parent
															//ou si le parent est la racine. Une fois la remontée terminée, le noeud est inséré en partant du frère et en descendant 
															//le plus à gauche possible. Si après la remontée le noeud stocké est la racine, on descend simplement à gauche 
															//(car la racine n'a pas de frère)
															
		E = tree->last;

		while (E != tree->root && E == E->parent->right) 
			E = E->parent;

		if (E != tree->root)
			E = E->parent->right;
		
		while (E->left != NULL)
				E = E->left;

		E->left = newTNode(data);
		E->left->parent = E;
		tree->last = E->left;
		tree->numelm++;
	}
}

void * CBTreeRemove(CBTree* tree) {
	assert(tree->last != NULL);

	void * ret;
	TNode * E;
	
	if (tree->numelm == 0) {	//Si l'arbre est vide, retourne NULL (aucun élément dans l'arbre)
		return NULL;
	} else if (tree->numelm == 1) {	//Si l'arbre ne contient que la racine, libération du noeud, root et last prennent la valeur NULL, le nombre d'élément tombe à 0 et retourne les données.
		ret = tree->root->data;
		free(tree->root);
		tree->root = NULL;
		tree->last = NULL;
		tree->numelm = 0;

		return ret;
	} else if (tree->last == tree->last->parent->right) {	//Si le noeud last est fils droit de son parent, le last devient le fils gauche.
		tree->last = tree->last->parent->left;
		ret = tree->last->parent->right->data;
		free(tree->last->parent->right);
		tree->last->parent->right = NULL;
		tree->numelm--;

		return ret;
	} else if (tree->last == tree->last->parent->left) {	//Si le noeud last est le fils gauche de son parent, remonter les parents tant que E est fils gauche ou différent de la racine.
															//Une fois la remontée terminée, on descend le plus à droite possible. E contient donc le nouveau last. L'échange est
															//effectué et les données retournées.
		E = tree->last;
		
		while (E != tree->root && E == E->parent->left)
			E = E->parent;

		if (E != tree->root)
			E = E->parent->left;

		while (E->right != NULL)
				E = E->right;

		ret = tree->last->data;
		tree->last->parent->left = NULL;
		free(tree->last);
		tree->last = E;
		tree->numelm--;
		
		return ret;
	} else {
		return NULL;
	}
}

void CBTreeSwap(CBTree* tree, TNode* parent, TNode* child) {
	assert(parent != NULL && child != NULL && (child == parent->left || child == parent->right));

	TNode * E;

	if (child != parent->left && child != parent->right)	//child doit être un enfant de parent
		return;
									//Différenciation des 2 cas : child est fils droit de parent ou fils gauche. L'algorithme change légèrement selon le cas.
									//Le nom des variables devrait être suffisant pour comprendre, c'est un simple échange de tous les pointeurs concernant child et parent
									//AINSI QUE les pointeurs "parent" des fils de child et parent
	if (child == parent->left) {		
		parent->left = child->left;
		if (child->left != NULL)			
			child->left->parent = parent;

		child->left = parent;

		if (child->right != NULL) {
			child->right->parent = parent;
			parent->right->parent = child;
		} else if (parent->right != NULL)
			parent->right->parent = child;

		E = parent->right;
		parent->right = child->right;
		child->right = E;

		child->parent = parent->parent;
	} else if (child == parent->right) {
		parent->right = child->right;
		if (child->right != NULL)
			child->right->parent = parent;

		child->right = parent;

		if (child->left != NULL) {
			child->left->parent = parent;
			parent->left->parent = child;
		} else if (parent->left != NULL)
			parent->left->parent = child;

		E = parent->left;
		parent->left = child->left;
		child->left = E;

		child->parent = parent->parent;
	}

	if (parent != tree->root) {
		if (parent->parent->left == parent)
			parent->parent->left = child;
		else
			parent->parent->right = child;
	} else {
		tree->root=child;
	}

	if (tree->last == child)
		tree->last = parent;
		
	parent->parent = child;
}

void CBTreeSwapRootLast(CBTree* tree) {
	TNode * E;

	if (tree->numelm == 0 || tree->numelm == 1)	{ //Dans ce cas aucun échange n'est possible
		return;
	} else {										//Échange des 2 noeuds, avec pour exception si l'arbre ne contient que 2 noeuds, on ne modifie pas le parent du fils droit de racine
													//car il n'existe pas
		tree->last->left = tree->root->left;
		tree->last->right = tree->root->right;
		tree->last->left->parent = tree->last;

		if (tree->numelm > 2)
			tree->last->right->parent = tree->last;

		if (tree->last == tree->last->parent->right)
			tree->last->parent->right = tree->root;
		else
			tree->last->parent->left = tree->root;

		tree->root->parent = tree->last->parent;
		tree->last->parent = NULL;
		tree->root->left = NULL;
		tree->root->right = NULL;
		E = tree->root;
		tree->root = tree->last;
		tree->last = E;
	}
}
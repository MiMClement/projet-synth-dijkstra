#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "list.h"
#include "test.h"
#include "tree.h"
#include "dyntable.h"
#include "heap.h"


static int compare_lists(List *l1, int l2[], int size) {
	if (l1->numelm != size)
		return 0;

	LNode *curr = l1->head;
	int i = 0;
	while (curr != NULL) {
		if ((*(int*)curr->data) != l2[i])
			return 0;
		curr = curr->suc;
		i++;
	}

	curr = l1->tail;
	i = size-1;
	while (curr != NULL) {
		if ((*(int*)curr->data) != l2[i])
			return 0;
		curr = curr->pred;
		i--;
	}
	return 1;
}

void test_list() {
	int t = 0;
	int f = 0;

	fprintf(stderr,"[I] Testing list functions : ");

	int *i1 = malloc(sizeof(int));
	int *i2 = malloc(sizeof(int));
	int *i3 = malloc(sizeof(int));
	int *i4 = malloc(sizeof(int));
	*i1 = 1;
	*i2 = 2;
	*i3 = 3;
	*i4 = 4;

	int tab[4] = {*i1,*i2,*i3,*i4};
	int t1[1] = {*i1};
	int t2[2] = {*i2,*i1};
	int t3[3] = {*i3,*i2,*i1};
	int t4[4] = {*i4,*i3,*i2,*i1};

	List *L = newList();
	listInsertLast(L, (int*) i1);
	if (compare_lists(L, tab, 1) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertLast(L, (int*) i2);
	if (compare_lists(L, tab, 2) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertLast(L, (int*) i3);
	if (compare_lists(L, tab, 3) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertLast(L, (int*) i4);
	if (compare_lists(L, tab, 4) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}

	deleteList(L, NULL);
	if (compare_lists(L, tab, 0) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}

	listInsertFirst(L, (int*) i1);
	if (compare_lists(L, t1, 1) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertFirst(L, (int*) i2);
	if (compare_lists(L, t2, 2) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertFirst(L, (int*) i3);
	if (compare_lists(L, t3, 3) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertFirst(L, (int*) i4);
	if (compare_lists(L, t4, 4) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}

	free(listRemoveFirst(L));
	if (compare_lists(L, t3, 3) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	free(listRemoveFirst(L));
	if (compare_lists(L, t2, 2) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	free(listRemoveFirst(L));
	if (compare_lists(L, t1, 1) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}

	deleteList(L, NULL);
	
	listInsertLast(L, (int*) i1);
	listInsertLast(L, (int*) i2);
	listInsertLast(L, (int*) i3);
	listInsertLast(L, (int*) i4);

	t1[0] = *i1;
	t2[0] = *i1;
	t2[1] = *i2;
	t3[0] = *i1;
	t3[1] = *i2;
	t3[2] = *i3;

	free(listRemoveLast(L));
	if (compare_lists(L, t3, 3) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	free(listRemoveLast(L));
	if (compare_lists(L, t2, 2) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	free(listRemoveLast(L));
	if (compare_lists(L, t1, 1) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	deleteList(L, NULL);

	listInsertLast(L, (int*) i1);
	listInsertLast(L, (int*) i2);
	listInsertLast(L, (int*) i3);
	listInsertLast(L, (int*) i4);

	t1[0] = *i1;
	t2[0] = *i1;
	t2[1] = *i4;
	t3[0] = *i1;
	t3[1] = *i2;
	t3[2] = *i4;

	free(listRemoveNode(L, L->head->suc->suc));
	if (compare_lists(L, t3, 3) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	free(listRemoveNode(L, L->head->suc));
	if (compare_lists(L, t2, 2) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	free(listRemoveNode(L, L->tail));
	if (compare_lists(L, t1, 1) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	deleteList(L, NULL);

	listInsertLast(L, (int*) i1);
	listInsertLast(L, (int*) i2);
	listInsertLast(L, (int*) i3);
	listInsertLast(L, (int*) i4);

	tab[0] = *i1;
	tab[1] = *i3;
	tab[2] = *i2;
	tab[3] = *i4;


	listSwap(L, L->head->suc, L->head->suc->suc);
	if (compare_lists(L, tab, 4) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listSwap(L, L->head->suc, L->head->suc->suc);
	tab[1] = *i2;
	tab[2] = *i3;
	if (compare_lists(L, tab, 4) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	tab[0] = *i2;
	tab[1] = *i1;
	listSwap(L, L->head, L->head->suc);
	if (compare_lists(L, tab, 4) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}


	fprintf(stderr,"\n[I] %d test(s) were successful and %d test(s) gave an error.\n", t, f);

	deleteList(L, NULL);

	free(i1);
	free(i2);
	free(i3);
	free(i4);
	free(L);
}

void testTree() {
	int t = 0;
	int f = 0;

	int * i1 = malloc(sizeof(int));
	int * i2 = malloc(sizeof(int));
	int * i3 = malloc(sizeof(int));
	int * i4 = malloc(sizeof(int)); 
	int * i5 = malloc(sizeof(int)); 
	int * i6 = malloc(sizeof(int));
	int * i7 = malloc(sizeof(int));
	int * i8 = malloc(sizeof(int));
	int * i9 = malloc(sizeof(int));
	int * i10 = malloc(sizeof(int));

	*i1 = 1;
	*i2 = 2;
	*i3 = 3;
	*i4 = 4;
	*i5 = 5;
	*i6 = 6;
	*i7 = 7;
	*i8 = 8;
	*i9 = 9;
	*i10 = 10;
	CBTree * T = newCBTree();

	fprintf(stderr,"[I] Testing tree functions : ");

	CBTreeInsert(T, i1);
	CBTreeInsert(T, i2);
	CBTreeInsert(T, i3);
	CBTreeInsert(T, i4);
	CBTreeInsert(T, i5);
	CBTreeInsert(T, i6);
	CBTreeInsert(T, i7);
	CBTreeInsert(T, i8);
	CBTreeInsert(T, i9);
	CBTreeInsert(T, i10);

	if (T->root->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->data == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->data == i3) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->left->data == i4) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->right->data == i5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->left->data == i6) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->right->data == i7) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->left->left->data == i8) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->left->right->data == i9) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->right->left->data == i10) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->last->data == i10) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->numelm == 10) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}


	CBTreeRemove(T);
	if (T->last->data == i9) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->numelm == 9) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	CBTreeRemove(T);
	if (T->last->data == i8) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	CBTreeRemove(T);
	if (T->last->data == i7) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	CBTreeRemove(T);
	if (T->last->data == i6) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	CBTreeRemove(T);
	if (T->last->data == i5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	CBTreeRemove(T);
	if (T->last->data == i4) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	CBTreeRemove(T);
	if (T->last->data == i3) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	CBTreeRemove(T);
	if (T->last->data == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	CBTreeRemove(T);
	if (T->last->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->numelm == 1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	CBTreeRemove(T);
	if (T->last == NULL) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->numelm == 0) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}

	CBTreeInsert(T, i1);
	CBTreeInsert(T, i2);
	CBTreeInsert(T, i3);
	CBTreeInsert(T, i4);
	CBTreeInsert(T, i5);
	CBTreeInsert(T, i6);
	CBTreeInsert(T, i7);
	CBTreeInsert(T, i8);
	CBTreeInsert(T, i9);
	CBTreeInsert(T, i10);

	CBTreeSwap(T, T->root->left, T->root->left->right);	//Échange de 2 et 5 
	if (T->root->left->data == i5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->left->data == i4) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->left->parent->data == i5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->right->data == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->right->parent->data == i5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->right->left->parent->data == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->right->left->data == i10) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->right->right == NULL) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	CBTreeSwap(T, T->root->left, T->root->left->right); //Échange de 5 et 2
	if (T->root->left->data == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->left->data == i4) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->left->parent->data == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->right->data == i5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->right->parent->data == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->right->left->parent->data == i5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->right->left->data == i10) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->right->right == NULL) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}

	CBTreeSwap(T, T->root->right, T->root->right->right);	//Échange de 3 et 7
	if (T->root->right->data == i7) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->right->data == i3) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->right->parent->data == i7) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->left->data == i6) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->left->parent->data == i7) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->left->left == NULL) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	CBTreeSwap(T, T->root->right, T->root->right->right);	//Échange de 7 et 3
	if (T->root->right->data == i3) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->right->data == i7) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->right->parent->data == i3) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->left->data == i6) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->left->parent->data == i3) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->left->left == NULL) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}

	CBTreeSwap(T, T->root, T->root->right); //Échange de 1 et 3
	if (T->root->data == i3) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->data == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->parent->data == i3) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->parent->data == i3) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->left->data == i6) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->left->parent->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->right->data == i7) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->right->parent->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	CBTreeSwap(T, T->root, T->root->right); //Échange de 3 et 1
	if (T->root->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->data == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->parent->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->data == i3) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->parent->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->left->data == i6) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->left->parent->data == i3) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->right->data == i7) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->right->parent->data == i3) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}

	CBTreeSwap(T, T->root, T->root->left); //Échange de 1 et 2
	if (T->root->data == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->data == i3) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->parent->data == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->parent->data == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->right->data == i5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->right->parent->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->left->data == i4) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->left->parent->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	CBTreeSwap(T, T->root, T->root->left); //Échange de 2 et 1
	if (T->root->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->data == i3) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->right->parent->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->data == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->parent->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->right->data == i5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->right->parent->data == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->left->data == i4) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}
	if (T->root->left->left->parent->data == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;}

	CBTreeSwapRootLast(T); //Échange de 1 et 10
	if (T->root->data == i10) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->root->parent == NULL) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->root->left->data == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->root->right->data == i3) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->root->left->parent->data == i10) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->root->right->parent->data == i10) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->last->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->last->left == NULL) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->last->right == NULL) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->last->parent->data == i5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->last->parent->left->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	CBTreeSwapRootLast(T); //Échange de 10 et 1
	if (T->root->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->root->parent == NULL) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->root->left->data == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->root->right->data == i3) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->root->left->parent->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->root->right->parent->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->last->data == i10) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->last->left == NULL) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->last->right == NULL) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->last->parent->data == i5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->last->parent->left->data == i10) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	for (int i = 0; i < 8; i++)
		CBTreeRemove(T);

	CBTreeSwapRootLast(T); //Échange de 1 et 2
	if (T->root->data == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->last->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->root->parent == NULL) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->last->parent == T->root) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->last->left == NULL) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->last->right == NULL) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->root->left->data == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (T->root->right == NULL) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	fprintf(stderr,"\n[I] %d test(s) were successful and %d test(s) gave an error.\n", t, f);

	
	CBTreeRemove(T);
	CBTreeRemove(T);
	free(T);
	free(i1);
	free(i2);
	free(i3);
	free(i4);
	free(i5);
	free(i6);
	free(i7);
	free(i8);
	free(i9);
	free(i10);
}

void testTab() {
	int t = 0;
	int f = 0;

	int * i1 = malloc(sizeof(int));
	int * i2 = malloc(sizeof(int));
	int * i3 = malloc(sizeof(int));
	int * i4 = malloc(sizeof(int)); 
	int * i5 = malloc(sizeof(int)); 
	int * i6 = malloc(sizeof(int));
	int * i7 = malloc(sizeof(int));
	int * i8 = malloc(sizeof(int));
	int * i9 = malloc(sizeof(int));
	int * i10 = malloc(sizeof(int));

	*i1 = 1;
	*i2 = 2;
	*i3 = 3;
	*i4 = 4;
	*i5 = 5;
	*i6 = 6;
	*i7 = 7;
	*i8 = 8;
	*i9 = 9;
	*i10 = 10;

	DTable *Tab = newDTable();

	fprintf(stderr,"[I] Testing tab functions : ");

	DTableInsert(Tab, i1);
	DTableInsert(Tab, i2);
	DTableInsert(Tab, i3);
	DTableInsert(Tab, i4);
	DTableInsert(Tab, i5);

	//Test Insert
	if (Tab->size == 8) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (Tab->used == 5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (Tab->T[Tab->used - 1] == i5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (Tab->T[Tab->used - 2] == i4) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (Tab->T[Tab->used - 3] == i3) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (Tab->T[Tab->used - 4] == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (Tab->T[Tab->used - 5] == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	DTableInsert(Tab, i6);
	DTableInsert(Tab, i7);
	DTableInsert(Tab, i8);
	DTableInsert(Tab, i9);
	DTableInsert(Tab, i10);

	//Test remove
	if (Tab->size == 16) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (Tab->used == 10) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (DTableRemove(Tab) == i10) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (Tab->used == 9) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (DTableRemove(Tab) == i9) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (DTableRemove(Tab) == i8) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (DTableRemove(Tab) == i7) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (DTableRemove(Tab) == i6) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (DTableRemove(Tab) == i5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (Tab->size == 16) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (DTableRemove(Tab) == i4) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (Tab->size == 8) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (DTableRemove(Tab) == i3) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (Tab->size == 8) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (DTableRemove(Tab) == i2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (Tab->size == 4) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (DTableRemove(Tab) == i1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (Tab->size == 2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (Tab->used == 0) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	DTableInsert(Tab, i6);
	DTableInsert(Tab, i7);
	DTableInsert(Tab, i8);
	DTableInsert(Tab, i9);
	DTableInsert(Tab, i10);

	DTableSwap(Tab, 0, 3);

	//Test swap
	if (Tab->T[0] == i9) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (Tab->T[3] == i6) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	DTableRemove(Tab);
	DTableRemove(Tab);
	DTableRemove(Tab);
	DTableRemove(Tab);
	DTableRemove(Tab);

	free(Tab->T);
	free(Tab);
	free(i1);
	free(i2);
	free(i3);
	free(i4);
	free(i5);
	free(i6);
	free(i7);
	free(i8);
	free(i9);
	free(i10);

	fprintf(stderr,"\n[I] %d test(s) were successful and %d test(s) gave an error.\n", t, f);
}

void testHeapList() {
	fprintf(stderr,"[I] Testing heap list functions : ");

	int t = 0;
	int f = 0;
	
	Heap * H = newHeap(2);
	HNode * E;
	LNode * L;

	OLHeapInsert(H, 2, NULL);
	OLHeapInsert(H, 5, NULL);
	OLHeapInsert(H, 1, NULL);
	OLHeapInsert(H, 9, NULL);
	OLHeapInsert(H, 0, NULL);

	if (((HNode *)((List *)H->heap)->head->data)->value == 0) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((List *)H->heap)->head->suc->data)->value == 1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((List *)H->heap)->head->suc->suc->data)->value == 2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((List *)H->heap)->head->suc->suc->suc->data)->value == 5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((List *)H->heap)->head->suc->suc->suc->suc->data)->value == 9) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	E = OLHeapExtractMin(H);
	if (E->value == 0) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((List *)H->heap)->head->data)->value == 1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	free(E);

	E = OLHeapExtractMin(H);
	if (E->value == 1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((List *)H->heap)->head->data)->value == 2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	free(E);

	E = OLHeapExtractMin(H);
	if (E->value == 2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((List *)H->heap)->head->data)->value == 5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	free(E);

	E = OLHeapExtractMin(H);
	if (E->value == 5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((List *)H->heap)->head->data)->value == 9) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	free(E);

	E = OLHeapExtractMin(H);
	if (E->value == 9) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	free(E);

	OLHeapInsert(H, 2, NULL);
	OLHeapInsert(H, 5, NULL);
	OLHeapInsert(H, 1, NULL);
	OLHeapInsert(H, 9, NULL);
	L = OLHeapInsert(H, 0, NULL);

	OLHeapIncreasePriority(H, L, 20);

	if (((HNode *)((List *)H->heap)->head->data)->value == 1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((List *)H->heap)->head->suc->data)->value == 2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((List *)H->heap)->head->suc->suc->data)->value == 5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((List *)H->heap)->head->suc->suc->suc->data)->value == 9) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((List *)H->heap)->head->suc->suc->suc->suc->data)->value == 20) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	E = OLHeapExtractMin(H);
	free(E);
	E = OLHeapExtractMin(H);
	free(E);
	E = OLHeapExtractMin(H);
	free(E);
	E = OLHeapExtractMin(H);
	free(E);
	E = OLHeapExtractMin(H);
	free(E);

	deleteList(H->heap, NULL);
	free(H->dict->T);
	free(H->dict);
	free(H->heap);
	free(H);

	fprintf(stderr,"\n[I] %d test(s) were successful and %d test(s) gave an error.\n", t, f);
}

void testHeapTree() {
	fprintf(stderr,"[I] Testing heap tree functions : ");

	int t = 0;
	int f = 0;
	
	Heap * H = newHeap(1);
	HNode * HN;
	void *v;

	CBTHeapInsert(H, 2, NULL);
	CBTHeapInsert(H, 5, NULL);
	CBTHeapInsert(H, 1, NULL);
	CBTHeapInsert(H, 9, NULL);
	CBTHeapInsert(H, 0, NULL);

	if (((HNode *)((CBTree *)H->heap)->root->data)->value == 0) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((CBTree *)H->heap)->root->data)->value < ((HNode *)((CBTree *)H->heap)->root->left->data)->value) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((CBTree *)H->heap)->root->data)->value < ((HNode *)((CBTree *)H->heap)->root->right->data)->value) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((CBTree *)H->heap)->root->left->data)->value < ((HNode *)((CBTree *)H->heap)->root->left->left->data)->value) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((CBTree *)H->heap)->root->left->data)->value < ((HNode *)((CBTree *)H->heap)->root->left->right->data)->value) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	HN = CBTHeapExtractMin(H);

	if (HN->value == 0) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((CBTree *)H->heap)->root->data)->value == 1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	free(HN);

	HN = CBTHeapExtractMin(H);

	if (HN->value == 1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((CBTree *)H->heap)->root->data)->value == 2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	free(HN);

	HN = CBTHeapExtractMin(H);

	if (HN->value == 2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((CBTree *)H->heap)->root->data)->value == 5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	free(HN);

	HN = CBTHeapExtractMin(H);

	if (HN->value == 5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((CBTree *)H->heap)->root->data)->value == 9) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	free(HN);

	HN = CBTHeapExtractMin(H);

	if (HN->value == 9) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if ((((CBTree *)H->heap)->root) == NULL) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	free(HN);

	CBTHeapInsert(H, 2, NULL);
	CBTHeapInsert(H, 5, NULL);
	CBTHeapInsert(H, 1, NULL);
	v = CBTHeapInsert(H, 9, NULL);
	CBTHeapInsert(H, 0, NULL);

	CBTHeapIncreasePriority(H, v, -10);

	HN = CBTHeapExtractMin(H);

	if (HN->value == -10) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((CBTree *)H->heap)->root->data)->value == 0) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	free(HN);

	HN = CBTHeapExtractMin(H);

	if (HN->value == 0) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((CBTree *)H->heap)->root->data)->value == 1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	free(HN);

	HN = CBTHeapExtractMin(H);

	if (HN->value == 1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((CBTree *)H->heap)->root->data)->value == 2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	free(HN);

	HN = CBTHeapExtractMin(H);

	if (HN->value == 2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if (((HNode *)((CBTree *)H->heap)->root->data)->value == 5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	free(HN);

	HN = CBTHeapExtractMin(H);

	if (HN->value == 5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	if ((((CBTree *)H->heap)->root) == NULL) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	free(HN);
	free(H->dict->T);
	free(H->dict);
	free(H->heap);
	free(H);

	fprintf(stderr,"\n[I] %d test(s) were successful and %d test(s) gave an error.\n", t, f);
}

void testHeapDynTab() {
	fprintf(stderr,"[I] Testing heap dynamic table functions : ");

	int t = 0;
	int f = 0;
	int *pos = malloc(sizeof(int));
	*pos = 3;

	Heap *H = newHeap(0);
	DTable *DT = H->heap;
	HNode *E;

	DTHeapInsert(H, 9, NULL);
	DTHeapInsert(H, 5, NULL);
	DTHeapInsert(H, 0, NULL);
	DTHeapInsert(H, 2, NULL);
	DTHeapInsert(H, 1, NULL);

	E = DT->T[0];
	if (E->value == 0) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	E = DT->T[1];
	if (E->value == 1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	E = DT->T[2];
	if (E->value == 5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	E = DT->T[3];
	if (E->value == 9) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};

	E = DT->T[4];
	if (E->value == 2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};



	E = DTHeapExtractMin(H);
	if (E->value == 0) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	free(E);

	E = DTHeapExtractMin(H);
	if (E->value == 1) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	free(E);

	E = DTHeapExtractMin(H);
	if (E->value == 2) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	free(E);

	E = DTHeapExtractMin(H);
	if (E->value == 5) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	free(E);

	E = DTHeapExtractMin(H);
	if (E->value == 9) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	free(E);

	DTHeapInsert(H, 9, NULL);
	DTHeapInsert(H, 5, NULL);
	DTHeapInsert(H, 0, NULL);
	DTHeapInsert(H, 2, NULL);
	DTHeapInsert(H, 1, NULL);

	DTHeapIncreasePriority(H, pos, -15);

	E = DTHeapExtractMin(H);
	if (E->value == -15) {fprintf(stderr,".");t++;} else {fprintf(stderr,"x");f++;};
	free(E);

	for (int i = 0; i < 4; i++)
		free(DTHeapExtractMin(H));

	free(pos);
	free(H->dict->T);
	free(H->dict);
	free(DT->T);
	free(DT);
	free(H);

	fprintf(stderr,"\n[I] %d test(s) were successful and %d test(s) gave an error.\n", t, f);
}
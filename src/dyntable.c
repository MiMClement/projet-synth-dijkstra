#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "dyntable.h"

DTable * newDTable() {
	DTable *E = malloc(sizeof(DTable));
	E->size = 1;
	E->used = 0;
	E->T = malloc(sizeof(void *) * E->size);

	return E;
}

/**
 * @brief
 * Dédoubler la taille du tableau dtab
 */
static void scaleUp(DTable *dtab) {
	dtab->size *= 2;
	dtab->T = realloc(dtab->T, sizeof(void *) * dtab->size);
}

/**
 * @brief
 * Diviser par 2 la taille du tableau dtab
 */
static void scaleDown(DTable *dtab) {
	if (dtab->size > 1) {
		dtab->size /= 2;
		dtab->T = realloc(dtab->T, sizeof(void *) * dtab->size);
	}
}

void viewDTable(const DTable *dtab, void (*ptrf)(const void*)) {
	int i;
	printf("size = %d\n", dtab->size);
	printf("used = %d\n", dtab->used);
	for (i = 0; i < dtab->used; i++) {
		ptrf(dtab->T[i]);
		printf(" ");
	}
	printf("\n");
}

void DTableInsert(DTable *dtab, void *data) {
	if (dtab->used < dtab->size) {
		dtab->T[dtab->used] = data;
		dtab->used++;
	} else {
		scaleUp(dtab);
		dtab->T[dtab->used] = data;
		dtab->used++;
	}
}

void * DTableRemove(DTable *dtab) {
	assert(dtab->used > 0);
	if (dtab->used == 0)
		return NULL;

	if (dtab->used > dtab->size / 4) {
		dtab->used--;
		return dtab->T[dtab->used];
	} else {
		scaleDown(dtab);
		dtab->used--;
		return dtab->T[dtab->used];
	}
	return NULL;
}

void DTableSwap(DTable *dtab, int pos1, int pos2) {
	assert(pos1 < dtab->used);
	assert(pos2 < dtab->used);
	void * tmp = dtab->T[pos1];
	dtab->T[pos1] = dtab->T[pos2];
	dtab->T[pos2] = tmp;
}
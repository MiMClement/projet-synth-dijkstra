#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "dyntable.h"
#include "tree.h"
#include "list.h"
#include "heap.h"
#include "town.h"
#include "road.h"
#include "graph.h"
#include "test.h"

int distBetweenTown(struct town *T1, struct town *T2) {
	LNode *path = T1->alist->head;

	while (path != NULL && strcmp(T2->name, getTownName(getURoad(path->data))) && strcmp(T2->name, getTownName(getVRoad(path->data))))
		path = path->suc;

	if (path == NULL)
		return INT_MAX;
	else
		return ((struct road*)path->data)->km;
}

List * neighborTown(struct town * src) {
	List *neighbor = newList();
	LNode *path = src->alist->head;

	while (path != NULL) {
		if (strcmp(getTownName(src), getTownName(getVRoad(path->data))))
			listInsertFirst(neighbor, ((struct road *)path->data)->V);
		else
			listInsertFirst(neighbor, ((struct road *)path->data)->U);

		path = path->suc;
	}

	return neighbor;
}

void initTowns(Heap *H, LNode *head, char *sourceName) {
	LNode *i;
	
	for (i = head;i != NULL; i = i->suc) {
		if (!strcmp(getTownName(i->data), sourceName))
			((struct town *)i->data)->dist = 0;
		
		((struct town *)i->data)->pred = NULL;
		((struct town *)i->data)->dist = INT_MAX;
	}

	for (i = head; i != NULL; i = i->suc)
		((struct town *)i->data)->ptr = H->HeapInsert(H,((struct town *)i->data)->dist, i->data);

		
}

/**
 * Exemple d'une fonction qui affiche le contenu d'un HNode.
 * A modifier si besoin.
 */
void viewHNode(const HNode *node) {
	struct town * town = node->data;
	printf("(%d, %s)", node->value, getTownName(town));
}

/**
 * Affichage de la solution de l'algorithme de Dijkstra.
 * Pour chaque ville du graphe G on doit déjà avoir défini
 * les valeurs dist et pred en exécutant l'algorithme de Dijkstra.
 */
void viewSolution(graph G, char * sourceName) {
	printf("Distances from %s\n", sourceName);
	LNode * iterator;
	for (iterator = G->head; iterator; iterator = iterator->suc) {
		if (strcmp(getTownName(iterator->data), sourceName) != 0)
			printf("%s : %d km (via %s)\n", getTownName((struct town *) iterator->data),
											((struct town *) iterator->data)->dist,
											getTownName(((struct town *) iterator->data)->pred));
	}
}

/**
 * Algorithme de Dijkstra
 * inFileName : nom du fichier d'entrée
 * outFileName : nom du fichier de sortie
 * sourceName : nom de la ville de départ
 * heaptype : type du tas {0--tableaux dynamiques, 1--arbres binaires complets, 2--listes ordonnées}
 */
void Dijkstra(char * inFileName, char * outFileName, char * sourceName, int heaptype) {
	Heap *H = newHeap(heaptype);
	graph G = readmap(inFileName);
	List *neighbor;
	int distFromCurrent, distFromNeighbor, pathDistBetweenTown;

	initTowns(H, G->head, sourceName);

	int numberTown = G->numelm;
	HNode *currentTown;

	while (numberTown > 0) {
		currentTown = H->HeapExtractMin(H);
		neighbor = neighborTown(currentTown->data);

		for (LNode *i = neighbor->head; i; i = i->suc) {
			distFromCurrent = ((struct town *)currentTown->data)->dist;
			distFromNeighbor = ((struct town *)i->data)->dist;
			pathDistBetweenTown = distBetweenTown(currentTown->data, i->data);

			if (distFromCurrent > (distFromNeighbor + pathDistBetweenTown)) {
				((struct town*)i->data)->dist = distFromNeighbor + pathDistBetweenTown;
				((struct town*)i->data)->pred = currentTown->data;

				H->HeapIncreasePriority(H, ((struct town*)i->data)->ptr, distFromNeighbor + pathDistBetweenTown);
			}
		}

		H->viewHeap(H, viewHNode);
		numberTown--;
	}

	viewSolution(G, sourceName);
}


int main() {
	Dijkstra("data/map2", "data/out", "Metz", 2);
	//test_list();
    //testTree();
	//testTab();
	//testHeapList();
	//testHeapTree();
	//testHeapDynTab();

	return EXIT_SUCCESS;
}
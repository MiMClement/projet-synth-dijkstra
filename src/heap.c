#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "dyntable.h"
#include "tree.h"
#include "list.h"
#include "heap.h"
#include "town.h"

HNode * newHNode(int value, void *data) {
	HNode * N = malloc(sizeof(HNode));
	N->data = data;
	N->value = value;
	N->ptr = NULL;
	return N;
}

// type =
//    0 (DynTableHeap)
//    1 (CompleteBinaryTreeHeap)
//    2 (ListHeap)
Heap * newHeap(int type) {
	assert(type == 0 || type == 1 || type == 2);
	Heap* H = calloc(1, sizeof(Heap));
	H->dict = newDTable();
	switch (type) {
		case 0:
			H->heap = newDTable();
			H->HeapInsert = DTHeapInsert;
			H->HeapExtractMin = DTHeapExtractMin;
			H->HeapIncreasePriority = DTHeapIncreasePriority;
			H->viewHeap = viewDTHeap;
			break;
		case 1:
			H->heap = newCBTree();
			H->HeapInsert = CBTHeapInsert;
			H->HeapExtractMin = CBTHeapExtractMin;
			H->HeapIncreasePriority = CBTHeapIncreasePriority;
			H->viewHeap = viewCBTHeap;
			break;
		case 2:
			H->heap = newList();
			H->HeapInsert = OLHeapInsert;
			H->HeapExtractMin = OLHeapExtractMin;
			H->HeapIncreasePriority = OLHeapIncreasePriority;
			H->viewHeap = viewOLHeap;
			break;
	}
	return H;
}

/**********************************************************************************
 * DYNAMIC TABLE HEAP
 **********************************************************************************/

void* DTHeapInsert(Heap *H, int value, void *data) {
	HNode *insert = newHNode(value, data);
	DTable *dtheap = H->heap;
	DTable *dict = H->dict;
	int pos, swapPos;

	DTableInsert(dtheap, insert);
	DTableInsert(dict, dtheap->T[dtheap->used]);

	insert->ptr = dtheap->T[dtheap->used];

	pos = dtheap->used - 1;
	swapPos = (pos - 1) / 2;

	while (pos > 0 && ((HNode *)dtheap->T[pos])->value < ((HNode *)dtheap->T[swapPos])->value) {
		DTableSwap(dtheap, pos, swapPos);
		pos = swapPos;
		swapPos = (pos - 1) / 2;
	}
	
	return dtheap->T[pos];
}

HNode * DTHeapExtractMin(Heap* H) {
	HNode *ret = ((DTable *)H->heap)->T[0];
	int pos, exchangePos;

	DTableSwap(H->heap, 0, ((DTable *)H->heap)->used-1);
	DTableRemove(H->heap);

	pos = 0;

	exchangePos = DTHeapMinPriorityChild(H->heap, pos);

	while (exchangePos != pos && exchangePos != -1) {
		DTableSwap(H->heap, pos, exchangePos);
		pos = exchangePos;
		exchangePos = DTHeapMinPriorityChild(H->heap, pos);
	}

	return ret;
}

void DTHeapIncreasePriority(Heap* H, void *position, int value) {
	int pos = *((int *)(position));
	int parent = (pos - 1) / 2;
	DTable *DT = H->heap;

	while (pos != 0 && value < ((HNode *)DT->T[parent])->value) {
		DTableSwap(DT, pos, parent);
		pos = parent;
		parent = (pos - 1) / 2;
	}

	((HNode *)DT->T[pos])->value = value;
}

void viewDTHeap(const Heap* H, void (*ptrf)(const void*)) {
	viewDTable(H->heap, ptrf);
}

int DTHeapMinPriorityChild(DTable *T, int pos) {
	int childLeft = 2*pos+1;
	int childRight = 2*pos+2;

	if (pos >= T->used)
		return -1;
	else if (childLeft >= T->used)
		return pos;
	else if (childRight >= T->used)
		return (((HNode *)T->T[childLeft])->value < ((HNode *)T->T[pos])->value) ? childLeft : pos;
	else if (((HNode *)T->T[childLeft])->value > ((HNode *)T->T[pos])->value && ((HNode *)T->T[childRight])->value > ((HNode *)T->T[pos])->value)
		return pos;
	else
		return (((HNode *)T->T[childLeft])->value < ((HNode *)T->T[childRight])->value) ? childLeft : childRight;
}

/**********************************************************************************
 * COMPLETE BINARY TREE HEAP
 **********************************************************************************/

void* CBTHeapInsert(Heap *H, int value, void *data) {
	TNode * T;
	
	if (((CBTree *)H->heap)->numelm == 0) {
		CBTreeInsert(H->heap, newHNode(value, data));
		return ((CBTree *)H->heap)->root->data;
	} else {
		CBTreeInsert(H->heap, newHNode(value, data));
		T = ((CBTree *)H->heap)->last;

		while (T->parent != NULL && ((HNode *)T->parent->data)->value > value) {
			CBTreeSwap(H->heap, T->parent, T);
		}

		return T->data;
	}
}

HNode * CBTHeapExtractMin(Heap *H) {
	HNode * ret;
	TNode * E;
	TNode * Ex;

	CBTreeSwapRootLast(H->heap);
	ret = CBTreeRemove(H->heap);
	
	E = ((CBTree *)H->heap)->root;

	Ex = CBTHeapMinPriorityChild(E);

	while (E != Ex && Ex != NULL) {
		CBTreeSwap(H->heap, E, Ex);
		Ex = CBTHeapMinPriorityChild(E);
	}
	
	return ret;
}

void CBTHeapIncreasePriority(Heap *H, void *tnode, int value) {
	((HNode *)((TNode *)tnode)->data)->value = value;

	TNode *T = tnode;

	while (T->parent != NULL && ((HNode *)T->parent->data)->value > value) {
			CBTreeSwap(H->heap, T->parent, T);
	}
}


void viewCBTHeap(const Heap *H, void (*ptrf)(const void*)) {
	viewCBTree(H->heap, ptrf, 0);
}

TNode * CBTHeapMinPriorityChild(TNode * P) {
	if (P == NULL)
		return NULL;
	else if (P->left == NULL)
		return P;
	else if (P->right == NULL)
		return (((HNode *)P->left->data)->value < ((HNode *)P->data)->value) ? P->left : P;
	else if (((HNode *)P->left->data)->value > ((HNode *)P->data)->value && ((HNode *)P->right->data)->value > ((HNode *)P->data)->value)
		return P;
	else
		return (((HNode *)P->left->data)->value < ((HNode *)P->right->data)->value) ? P->left : P->right;
}

/**********************************************************************************
 * ORDERED-LIST HEAP
 **********************************************************************************/

void* OLHeapInsert(Heap *H, int value, void* data) {
	HNode * N = newHNode(value, data);
	LNode * L = ((List *)H->heap)->head;
	
	if (((List *)H->heap)->numelm == 0) {
		listInsertFirst(H->heap, N);
		return ((List *)H->heap)->head->data;
	} else if (((HNode *)((List *)H->heap)->head->data)->value > value) {
		listInsertFirst(H->heap, N);
		return ((List *)H->heap)->head->data;
	} else {
		while (L->suc != NULL && ((HNode *)L->data)->value < value) {
			L = L->suc;
		}

		listInsertAfter(H->heap, N, L);

		return L->suc->data;
	}
}

HNode * OLHeapExtractMin(Heap *H) {
	LNode * L;
	HNode * E;
	
	if (((List *)H->heap)->numelm == 0)
		return NULL;
	else {
		L = listRemoveFirst(H->heap);
		E = L->data;
		free(L);
		return E;
	}
}

void OLHeapIncreasePriority(Heap *H, void* lnode, int value) {
	((HNode *)((LNode *)lnode)->data)->value = value;
	
	if (((LNode *)lnode)->suc == NULL) {
		return;		//la priorité ne peut être qu'augmenter, pas besoin de verification donc
	} else {
		while (((LNode *)lnode)->suc != NULL && ((HNode *)((LNode *)lnode)->data)->value > ((HNode *)((LNode *)lnode)->suc->data)->value) {
			listSwap(H->heap, lnode, ((LNode *)lnode)->suc);
		}
	}
}

void viewOLHeap(const Heap *H, void (*ptrf)()) {
	viewList(H->heap, ptrf);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "list.h"

List * newList() {
	List * L = calloc(1, sizeof(List));
	return L;
}

void deleteList(List * L, void (*ptrF)()) {
	LNode * iterator = L->head;

	if (ptrF == NULL) {
		while (iterator) {
			LNode * current = iterator;

			iterator = iterator->suc;
			free(current);
		}
	} else {
		while (iterator) {
			LNode * current = iterator;

			(*ptrF)(&current->data);
			free(current);
		}
	}
	L->head = NULL;
	L->tail = NULL;
	L->numelm = 0;
}

void viewList(const List * L, void (*ptrF)()) {
	printf("number of nodes = %d\n", L->numelm);

	if (ptrF == NULL)
		return;

	for(LNode * iterator = L->head; iterator; iterator = iterator->suc) {
		(*ptrF)(iterator->data);
		printf(" ");
	}
}

void listInsertFirst(List * L, void * data) {
	LNode * E = calloc(1, sizeof(LNode));

	E->data = data;
	E->suc = L->head;

	if(L->head == NULL)
		L->tail = E;
	else
		L->head->pred = E;
	L->head = E;
	L->numelm += 1;
}

void listInsertLast(List * L, void * data) {
	LNode * E = calloc(1, sizeof(LNode));

	E->data = data;
	E->pred = L->tail;

	if(L->tail == NULL)
		L->head = E;
	else
		L->tail->suc = E;
	L->tail = E;
	L->numelm += 1;
}

void listInsertAfter(List * L, void * data, LNode * ptrelm) {
	LNode * E = calloc(1, sizeof(LNode));

	E->data = data;
	E->pred = ptrelm;

	if(ptrelm == L->tail)
		L->tail = E;
	else
		ptrelm->suc->pred = E;
	E->suc = ptrelm->suc;
	ptrelm->suc = E;
	L->numelm += 1;
}

LNode* listRemoveFirst(List * L) {
	assert(L->head);

	LNode *E;

	E = L->head;
	if (L->numelm == 1) {		//Si la liste ne contient qu'un seul élément, la liste devient vide (head et tail à NULL, numelm à 0)
		L->head = NULL;
		L->tail = NULL;
		L->numelm = 0;
	}
	else if (L->numelm > 1) {		//Sinon, la tête de liste devient son successeur, 
									//le prédecesseur de la nouvelle tête devient NULL, 
									//l'ancienne tête perd son successeur et le nombre d'éléments décrémente
		L->head = L->head->suc;
		L->head->pred = NULL;
		E->suc = NULL;
		L->numelm--;
	}
	
	return E;
}

LNode* listRemoveLast(List * L) {
	assert(L->head);
	
	LNode *E;

	E = L->tail;
	if (L->numelm == 1) {
		L->head = NULL;
		L->tail = NULL;
		L->numelm = 0;
	}
	else if (L->numelm > 1) {
		L->tail = L->tail->pred;
		L->tail->suc = NULL;
		E->pred = NULL;
		L->numelm--;
	} else {
		fprintf(stderr, "[E] listRemoveLast() : Liste avec un nombre d'élément non valide (< 0)\n");
		return NULL;
	}
	
	return E;
}

LNode* listRemoveNode(List * L, LNode * node) {
	if (node == L->head)
		return listRemoveFirst(L);
	if (node == L->tail)
		return listRemoveLast(L);

	node->pred->suc = node->suc;
	node->suc->pred = node->pred;
	node->suc = NULL;
	node->pred = NULL;
	L->numelm--;

	return node;
}

void listSwap(List * L, LNode * left, LNode * right) {
	assert(left->suc == right && left == right->pred);

	if (left == L->head) {
		left->pred = right;
		left->suc = right->suc;
		left->suc->pred = left;
		right->pred = NULL;
		right->suc = left;
		L->head = right;
	} else if (right == L->tail) {
		left->pred->suc = right;
		left->suc = NULL;
		right->pred = left->pred;
		left->pred = right;
		right->suc = left;
		L->tail = left;
	} else {
		left->pred->suc = right;
		right->suc->pred = left;
		left->suc = right->suc;
		right->pred = left->pred;
		right->suc = left;
		left->pred = right;
	}
}
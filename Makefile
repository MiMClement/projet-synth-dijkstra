IDIR = include
ODIR = obj
SDIR = src

CC = gcc
CFLAGS = -Wno-unused-function -g -Wall -std=c11 -I$(IDIR)
#CFLAGS = -g -Wall -Wextra -std=c11 -I$(IDIR)

PROG = dijkstra

_DEPS = dyntable.h tree.h list.h heap.h town.h road.h graph.h test.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ= dyntable.o tree.o list.o heap.o town.o road.o graph.o test.o main.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

.PHONY: run all remake clean delete

all : $(PROG)

$(PROG) : $(OBJ)
	$(CC) $(CFLAGS) -o $@ $^

$(ODIR)/%.o : $(SDIR)/%.c $(DEPS)
	$(CC) $(CFLAGS) -c -o $@ $<

run : all
	./$(PROG)

clean :
	rm -f $(ODIR)/*.o

delete : clean
	rm $(PROG)
